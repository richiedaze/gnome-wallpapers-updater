#!/usr/bin/env bash

# Set it an forget it

function Regenerate_Wallpapers_Xml () 
{ 
    local Xdg_Data_Home="${XDG_DATA_HOME:-$HOME/.local/share}";
    local Gnome_Backgrnd_Prop_Dir="${Xdg_Data_Home}/gnome-background-properties";
    local Xdg_Wallpapers_Dir="${XDG_PICTURES_DIR:-$HOME/Pictures}/Wallpapers";
    local Wallpapers_Dir="${Xdg_Wallpapers_Dir}";
    Wallpapers=$(find "${Wallpapers_Dir}" -regex ".*\.\(jpg\|jpeg\|png\)" | sort | xargs -i echo -e " <wallpaper>
    <name>{}</name>
    <filename>{}</filename>
    <options>zoom</options>
    <pcolor>#000000</pcolor>
    <scolor>#000000</scolor>
    <shade_type>solid</shade_type>
 </wallpaper>");
    [[ -d $Gnome_Backgrnd_Prop_Dir ]] || /usr/bin/mkdir --parents "${Gnome_Backgrnd_Prop_Dir}" 
    cat <<Updated_Wallpaper_Xml > "${Gnome_Backgrnd_Prop_Dir}"/wallpapers.xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd">
<!--  Any modifications done to the wallpapers directory 
      will regenerate this xml file-->
<wallpapers>
${Wallpapers}
</wallpapers>
Updated_Wallpaper_Xml
}


function Create_Regenerate_Wallpapers_Xml_Script()
{
    local Xdg_Bin_Home=${XDG_BIN_HOME:-$HOME/.local/bin}
    local Xdg_Wallpapers_Dir="${XDG_PICTURES_DIR:-$HOME/Pictures}/Wallpapers";
    Add_Functions=$(declare -f Regenerate_Wallpapers_Xml)
    [[ -d $Xdg_Wallpapers_Dir ]] || /usr/bin/mkdir --parents "${Xdg_Wallpapers_Dir}"
    [[ -d $Xdg_Bin_Home ]] || /usr/bin/mkdir --parents "${Xdg_Bin_Home}" 
    cat << Script > "${Xdg_Bin_Home}"/regenerate-wallpapers-xml-for-you
#!/usr/bin/env bash

$Add_Functions

Update_Available_Wallpapers(){
#Add New Wallpapers To The Xml
  Regenerate_Wallpapers_Xml 
} ; Update_Available_Wallpapers
Script
    chmod +x "${Xdg_Bin_Home}"/regenerate-wallpapers-xml-for-you
}
 
 
function Select_Random_Wallpaper () 
{ 
    local Xdg_Wallpapers_Dir="${XDG_PICTURES_DIR:-$HOME/Pictures}/Wallpapers";
    local Wallpapers_Entries=("${Xdg_Wallpapers_Dir}"/{*,*/*,*/*/*}{.jpg,.png});
    local Random_Wallpaper="${Wallpapers_Entries[RANDOM % ${#Wallpapers_Entries[@]}]}";
    /usr/bin/gsettings set org.gnome.desktop.background picture-uri file:///"$Random_Wallpaper"
}


function Create_Select_Random_Wallpaper_Script()
{
    local Xdg_Bin_Home=${XDG_BIN_HOME:-$HOME/.local/bin}
    local Xdg_Wallpapers_Dir="${XDG_PICTURES_DIR:-$HOME/Pictures}/Wallpapers";
    Add_Functions=$(declare -f Select_Random_Wallpaper)
    [[ -d $Xdg_Bin_Home ]] || /usr/bin/mkdir --parents "${Xdg_Bin_Home}" 
    [[ -d $Xdg_Wallpapers_Dir ]] || /usr/bin/mkdir --parents "${Xdg_Wallpapers_Dir}"
    cat << Script > "${Xdg_Bin_Home}"/random-wallpaper
#!/usr/bin/env bash

$Add_Functions

Change_Background(){
#Search Xdg_Wallpapers_Dir
  Select_Random_Wallpaper 
} ; Change_Background
Script
    chmod +x "${Xdg_Bin_Home}"/random-wallpaper
}


function Create_Reload_Wallpapers_Service () 
{ 
    local XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}";
    local Systemd_Usr_Dir="${XDG_CONFIG_HOME}/systemd/user";
    [[ -d $Systemd_Usr_Dir ]] || /usr/bin/mkdir --parents "${Systemd_Usr_Dir}" 
    cat <<Systemd_Service > "${Systemd_Usr_Dir}"/reload-wallpapers.service
[Unit]
Description=reload-wallpapers.service

[Service]
Type=oneshot
ExecStart=%h/.local/bin/regenerate-wallpapers-xml-for-you

[Install]
WantedBy=default.target
Systemd_Service
}



function Create_Reload_Wallpapers_Service_Path () 
{ 
    local XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}";
    local Systemd_Usr_Dir="${XDG_CONFIG_HOME}/systemd/user";
    [[ -d $Systemd_Usr_Dir ]] || /usr/bin/mkdir --parents "${Systemd_Usr_Dir}" 
    cat <<Systemd_Path > "${Systemd_Usr_Dir}"/reload-wallpapers.path
[Unit]
Description=reload-wallpapers.path

[Path]
PathChanged=%h/Pictures/Wallpapers

[Install]
WantedBy=default.target
Systemd_Path
}

function Enable_And_Start_Reload_Wallpapers_Path_Service()
{
 systemctl --user daemon-reload
 systemctl --user enable reload-wallpapers.path
 systemctl --user start reload-wallpapers.path
 systemctl --user status reload-wallpapers.path
}


# Create Scripts With Execution Premission 
   Create_Select_Random_Wallpaper_Script
   Create_Regenerate_Wallpapers_Xml_Script

# Create Service Files
if [[ "type -p systemd" ]]; then
   Create_Reload_Wallpapers_Service
   Create_Reload_Wallpapers_Service_Path

# Enable and Start Service For Automation
   Enable_And_Start_Reload_Wallpapers_Path_Service
fi

