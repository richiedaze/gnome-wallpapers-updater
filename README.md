# GNOME wallpapers updater

This script automatically generates a wallpapers.xml in `"${XDG_DATA_HOME}/gnome-background-properties"` folder when you add,delete or move .png/jpeg wallpapers from the `"${XDG_PICTURES_DIR}/Wallpapers"` folder up to 3 folder level deep so you can organize your wallapers. 

# This script sets up:
- Bash script for generating an xml in gnome-background-properites 
- Bash script for a random wallpaper from wallpapers directories (type random-wallpaper from terminal)

# If your OS uses systemd:
- Creates a service file in user/systemd to execute the script to regenerate wallpapers.xml
- Creates a service path file to monitor the wallpapers directories of changes, which executes the service file
- Tells systemd to reload the daemon so that it's aware of the new service files
- Tells systemd to enable the service path file so it starts on boot
- Tells systemd to start the monitoring of the service path 
- Tells systemd to show you the status of the service (press q to exit from terminal)


# Please read the bash script before running this command.
running this command from the terminal will automatically set up each file in your system for automation without downloading it

`bash <(wget -qO- https://gitlab.com/richiedaze/gnome-wallpapers-updater/-/raw/master/gnome-wallpapers-updater.sh)`




# Help me improve this script! Constructive criticism always welcomed